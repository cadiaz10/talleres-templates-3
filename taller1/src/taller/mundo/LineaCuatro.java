package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Número de fichas seguidas con las que se gana el juego
	 */
	private int fichas;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFich)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		fichas = pFich;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		// Selecciona una columna al azar
		Random r = new Random();
		int col = r.nextInt(tablero[0].length);
		
		boolean terminar = false;
		int row = tablero.length - 1;
		String punto = null;
		
		while (row >= 0 && !terminar){
			punto = tablero[row][col];
			if (punto.equals("___")){
				tablero[row][col] = jugadores.get(1).darSimbolo();
				cambiarTurno();
				terminar = true;
				finJuego = terminar(row, col);
			}
			
			if (row == 0) {
				row = tablero.length;
				col = r.nextInt(tablero[0].length);
			}
			row --;
		}
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean registrar = false;
		String punto = null;
		int row = tablero.length - 1;
		
		while (row >= 0 && !registrar){
			punto = tablero[row][col];
			if (punto.equals("___")){
				tablero[row][col] = jugadores.get(turno).darSimbolo();
				cambiarTurno();				
				registrar = true;
				finJuego = terminar(row, col);
			}
			row --;
		}
		return registrar;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		boolean termino = false;

		return termino;
	}
	
	/**
	 * Cambia el turno
	 */
	public void cambiarTurno(){
		if (turno + 1 >= jugadores.size()){
			turno = 0;
			atacante = jugadores.get(turno).darNombre();
		} else{
			turno ++;
			atacante = jugadores.get(turno).darNombre();
		}
	}
}
