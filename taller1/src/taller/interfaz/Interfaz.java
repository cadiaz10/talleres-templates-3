package taller.interfaz;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Cantidad de filas del tablero
	 */
	private int filas;
	
	/**
	 * Cantidad de columnas del tablero
	 */
	private int columnas;
	
	/**
	 * Cantidad de fichas
	 */
	private int fichas;
	/**
	 * 
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		filas = 0; columnas = 0; fichas = 0;
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		// Pide la cantidad de jugadores que van a jugar	
		System.out.println("Ingrese la cantidad de jugadores: ");
		int jugadores = Integer.parseInt(sc.next());
		while (jugadores < 2){
			System.out.println("El número mínimo de jugadores es 2");
			System.out.println("Ingrese el número de jugadores: ");
			jugadores = Integer.parseInt(sc.next());
		}
		
		// Pide los nombres y el símbolo de cada uno de los jugadores
		
		ArrayList<Jugador> jug = new ArrayList<Jugador>();
		for (int i=1; i <= jugadores; i++){
			System.out.println("Ingrese el nombre del jugador " + i + ": ");
			String nombre = sc.next();
			System.out.println("Ingrese el simbolo del jugador " + i + ": ");
			System.out.println("Nota: El símbolo debe ser una letra o un número y no debe repetirse");
			String simbolo = sc.next();
			Jugador j1 = new Jugador(nombre, simbolo);
			jug.add(j1);
		}

		// Se crea el tablero de acuerdo al usuario
		tamanoTablero();
		
		// Crea el juego nuevo
		juego = new LineaCuatro(jug, filas, columnas, fichas);
		
		// Jugar
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		// Dice quien tiene el primer turno
		System.out.println(juego.darAtacante() + " tiene el primer turno!");
		// Imprime el tablero inicial
		imprimirTablero();
		System.out.println("");
		
		while (!juego.fin()){
			System.out.println("Digite la columna en donde desea poner la ficha");
			int column = Integer.parseInt(sc.next());
			while (column > columnas){
				System.out.println("El número de columna no existe");
				System.out.println("Ingrese la columna en donde desea poner la ficha");
				column = Integer.parseInt(sc.next());
			}
			boolean registro = juego.registrarJugada(column - 1);
			while (!registro){
				System.out.println("No se puede registrar la jugada en la columna escogida");
				System.out.println("Ingrese una columna: ");
				column = Integer.parseInt(sc.next());
				registro = juego.registrarJugada(column - 1);
			}
			imprimirTablero();
			System.out.println("");
			System.out.println(juego.darAtacante() + " tiene el turno");
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		// Se crea la matriz de jugadores
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
				
		// Pregunta el nombre y el simbolo del jugador para luego crearlo
		System.out.println("Ingrese el nombre");
		String nombre = sc.next();
		System.out.println("Ingrese el símbolo");
		String simbolo = sc.next();
		Jugador j1 = new Jugador(nombre, simbolo);
		jugadores.add(j1);
		
		// Crea los datos del computador. Se selecciona un simbolo diferente al escogido por el usuario.
		String nombrePC = "Computador";
		Random r = new Random();
		char c = (char) (r.nextInt(26) + 'a');
		String simboloPC = Character.toString(c);
		while (simboloPC.equals(simbolo)){
			c = (char) (r.nextInt(26) + 'a');
			simboloPC = Character.toString(c);
		}
		Jugador j2 = new Jugador(nombrePC, simboloPC);
		jugadores.add(j2);
		
		// Pregunta el tamano del tablero
		tamanoTablero();
		
		// Crea el juego
		juego = new LineaCuatro(jugadores, filas, columnas, fichas);
		
		// Jugar
		juegoMaquina();
		
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		// Dice quien tiene el primer turno
		System.out.println(juego.darAtacante() + " tiene el primer turno!");
		// Imprime el tablero inicial
		imprimirTablero();
		System.out.println("");
		
		while (!juego.fin()){
			if(!juego.darAtacante().equals("Computador")){
				System.out.println("Digite la columna en donde desea poner la ficha");
				int column = Integer.parseInt(sc.next());
				while (column > columnas){
					System.out.println("El número de columna no existe");
					System.out.println("Ingrese la columna en donde desea poner la ficha");
					column = Integer.parseInt(sc.next());
				}
				boolean registro = juego.registrarJugada(column - 1);
				while (!registro){
					System.out.println("No se puede registrar la jugada en la columna escogida");
					System.out.println("Ingrese una columna: ");
					column = Integer.parseInt(sc.next());
					registro = juego.registrarJugada(column - 1);
				}
				imprimirTablero();
				System.out.println("");
				System.out.println(juego.darAtacante() + " tiene el turno");
			} else {
				juego.registrarJugadaAleatoria();
				imprimirTablero();
			}
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		for (int i = 0; i<juego.darTablero().length; i++){
			String imp="";
			for (int j=0; j < juego.darTablero()[0].length; j++){
				imp = imp + " " + juego.darTablero()[i][j];
			}
			System.out.println(imp);
		}
	}
	
	/**
	 * Define el tamano del tablero
	 */
	public void tamanoTablero(){
		//Toma el número de filas del tablero
		System.out.println("Tamano del tablero (filas x columnas): ");
		System.out.println("Tenga en cuenta que el tamanio minimo es 4 x 4");
		System.out.println("Ingrese la cantidad de filas: ");
		filas = Integer.parseInt(sc.next());
		while (filas <4){
			System.out.println("El número de filas es menor a 4");
			System.out.println("Ingrese el número de filas: ");
			filas = Integer.parseInt(sc.next());
		}
				
		//Toma el número de columnas del tablero
		System.out.println("Ingrese la cantidad de columnas: ");
		columnas = Integer.parseInt(sc.next());
		while (columnas < 4){
			System.out.println("El número de columnas es menor a 4");
			System.out.println("Ingrese el número de columnas: ");
			columnas = Integer.parseInt(sc.next());
		}
		
		// Pregunta al usuario con cuantas fichas seguidas se puede ganar
		System.out.println("Ingrese el número de fichas seguidas con las que se gana el juego: ");
		fichas = Integer.parseInt(sc.next());
		
	}
}
